# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.0.1] - 2022-07-16

### Added
- bit shifting macros rq_pid2sbn(3), rq_pid2esi(3) rq_pidsetsbn(3) rq_pidsetesi(3)

### Fixed
- CID 274791: Out-of-bounds access
- CID 274784: Unintended sign extension
- CID 274785: Unintended sign extension
- CID 274790: Unintended sign extension
- CID 274793: Unintended sign extension
- CID 274794: Unintended sign extension
- CID 274789: Out-of-bounds read
- CID 274792: Resource leak
- fix big endian bug in rq_symbol(3)
- update examples/ to use renamed API calls

## [0.0.0.0] - 2022-07-14

### Added
- install note for *BSD

### Changed
- remove RFC 6330 from docs due to "non-free" licence to make packaging easier.

## [0.0.0] - 2022-07-12 Initial release

C library RFC6330 RaptorQ Implementation for Librecast
