# SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only
# Copyright (c) 2022 Brett Sheffield <bacs@librecast.net>

PREFIX ?= /usr/local
export PREFIX
LIBNAME := lcrq
LIBDIR := $(PREFIX)/lib
INCLUDEDIR := $(PREFIX)/include
COVERITY_DIR := cov-int
COVERITY_TGZ := $(LIBNAME).tgz

ifeq ($(origin CC),default)
CC = @CC@
endif
export CC

CFLAGS ?= @CFLAGS@
ifdef DEBUG
CFLAGS += -g -Og
endif
export CFLAGS

CPPFLAGS += @CPPFLAGS@
export CPPFLAGS

all: src

install: all doc
	cd src && $(MAKE) $@
	cd doc && $(MAKE) $@

uninstall:
	cd src && $(MAKE) $@

.PHONY: clean realclean src test sparse examples

src examples:
	$(MAKE) -C $@

fixme:
	grep -n FIXME src/*.{c,h} test/*.{c,h}

todo:
	grep -n TODO src/*.{c,h} test/*.{c,h}

speedtest: src
	cd test && $(MAKE) $@

clean realclean:
	cd src && $(MAKE) $@
	cd test && $(MAKE) $@
	rm -rf ./$(COVERITY_DIR)
	rm -f $(COVERITY_TGZ)

sparse: clean
	CC=cgcc $(MAKE) src

clang: clean
	CC=clang $(MAKE) src

clangtest: clean
	CC=clang $(MAKE) test

gcc: clean all

check test sanitize: src
	cd test && $(MAKE) $@

%.test %.check %.debug: src
	cd test && $(MAKE) $@

coverity: clean
	PATH=$(PATH):../coverity/bin/ cov-build --dir cov-int $(MAKE) src
	tar czvf $(COVERITY_TGZ) $(COVERITY_DIR)
